import fileInclude from 'gulp-file-include';

export const html = () => {
  return $.gulp
    .src($.path.src.html)
    .pipe(fileInclude())
    .pipe($.plugins.replace(/@img\//g, 'img/'))
    .pipe($.gulp.dest($.path.build.html));
};
